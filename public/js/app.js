$(document).ready(function() {
	$('.toggle').click(function(event) {
		event.preventDefault();
		var url = $(this).val();
		$.ajax({
			type: "GET",
			url: url,
			data: {
				name: name,
			},
			success: function(data) {
				/** Image */
				url = data['sprites']['front_default'];
				$('.sprite_container').append("<img src=" + url + ">");

				/** Basic Information */
				$('.modal-title').html(data['name']);
				$('.species').append(data['species']['name']);
				$('.height').append(data['height']);
				$('.weight').append(data['weight']);
				$('.abilities').append(data['abilities'][0]['ability']['name']);
				
				/** Statistics */
				$('.speed').append(data['stats'][0]['base_stat']);
				$('.special-defence').append(data['stats'][1]['base_stat']);
				$('.special-attack').append(data['stats'][2]['base_stat']);
				$('.defence').append(data['stats'][3]['base_stat']);
				$('.attack').append(data['stats'][4]['base_stat']);
				$('.hp').append(data['stats'][5]['base_stat'])
			},
			error: function(err) {
				console.log("Error:" + JSON.stringify(err));
			}

		});
	});

	$('.close').click(function() {
		$('.modal-title').html("");
		$('.sprite_container').html("");
		$('.species').html("");
		$('.species').append("Species: ");
		$('.height').html("");
		$('.height').append("Height: ");
		$('.weight').html("");
		$('.weight').append("Weight: ");
		$('.abilities').html("");
		$('.abilities').append("Abilities: ");
		$('.speed').html("");
		$('.speed').append("Speed: ");
		$('.special-defence').html("");
		$('.special-defence').append("Special Defence: ");
		$('.special-attack').html("");
		$('.special-attack').append("Special Attack: ");
		$('.defence').html("");
		$('.defence').append("Defence: ");
		$('.attack').html("");
		$('.attack').append("Attack: ");
		$('.hp').html("");
		$('.hp').append("Hitpoints: ");
	});
});
