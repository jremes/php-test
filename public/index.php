<?php 

require_once('../bootstrap.php'); 
require_once('../src/class/PokeDex.class.php');
require_once('../src/class/Security.class.php');

$pokedex = new PokeDex();
$security = new Security();

if (isset($_GET['search'])) {
    $getSearch = $security->sanitize($_GET['search']);
    $searchPokemon = $pokedex->searchPokemon(strtolower($getSearch));
} else {
    $getSearch = null;
}

$total = $pokedex->getTotalPokemon();
$dex = $pokedex->getAllPokemon();
echo $twig->render('index.html', ['dex' => $dex['results'], 'total' => $total, 'searchPokemon' => $searchPokemon]);

?>
