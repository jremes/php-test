<?php 

class Security 
{
	/**
	 * Sanitizes the user input to prevent SQL injection 
	 * and XSS. Returns the sanitized search parameter.
	 */
	public function sanitize($param)
	{
		return htmlspecialchars($param, ENT_QUOTES, 'UTF-8');
	}
}