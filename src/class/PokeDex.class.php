<?php

class PokeDex 
{
	protected $url = "https://pokeapi.co/api/v2/pokemon/";

	/**
	 * Constructor method for PokeDex class.
	 */
	public function __construct($url = false) 
	{
		if($url) {
			$this->url = $url;
		}
	}

	/**
	 * Fetches all the pokemons from remote API.
	 */
	public function getAllPokemon()
	{
		$total = $this->getTotalPokemon();
		$getPokemons = file_get_contents($this->url."?limit=".$total);
		$pokemonArray = json_decode($getPokemons, true);
		return $pokemonArray;
	}

	/**
	 * Counts the total number of Pokemons.
	 */
	public function getTotalPokemon()
	{
		$totalCount = file_get_contents($this->url);
		$countArray = json_decode($totalCount, true);
		$grandTotal = $countArray['count'];
		return $grandTotal;
	}

	/**
	 * Function for the search.
	 */
	public function searchPokemon($name)
	{
		$search = file_get_contents($this->url.$name);
		$searchArray = json_decode($search, true);
		return $searchArray;
	}
}

?>