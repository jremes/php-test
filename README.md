# ukfast/pokédex

**This project will be based primarily on your ability to fulfill the task 
requirements. Any potential design skills are a bonus, but usability, 
performance and security will be taken into account.**

## Introduction
This project provides a starting point which will allow you to create your own 
web-based encyclopedia based on the popular franchise Pokémon - also known as 
a pokédex.

## Project Requirements
To get started, you'll need the following:

 - PHP
 - [Composer](https://getcomposer.org/)
 - git
 
 You are free to use whatever PHP packages and front-end libraries that you 
 wish.

## Task Requirements
To order to complete this challenge, you MUST create a pokédex with minimal 
functionality. Your solution MUST allow the user to browse the full list of 
pokémon in a convenient manner, as well as offer some form of search 
functionality. Your solution MUST also display basic information for a 
specific pokémon, including:

 - At least one image of the pokémon
 - Name
 - Species
 - Height and weight
 - Abilities
 
A RESTful API is available at [Pokéapi](https://pokeapi.co/) which will 
provide you with all the data that you will need. You do not need to create 
an account nor authenticate in order to consume the API, however please 
be aware that this API is rate-limited.
 
To get started, we've given you a skeleton folder structure. It is advised 
that you spend no more than two to three hours on this assignment.
 
## Submission
To submit your solution, please fork this repository and provide us a link 
to your finished version.

## Copyright
All trademarks as the property of their respective owners.

# Author's Comments 

## Dependencies

I used Twig as a template engine, as it makes a bit easier to write and render templates. 

## Technologies

I didn't use any frameworks for this task, just a basic composer setup and Twig. The buttons are working through AJAX, and the search bar uses it's own method in the back-end.

## Installation 

Just clone the repo, run composer install and enjoy.